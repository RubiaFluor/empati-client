import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { MainEncuestaComponent } from './modules/encuesta/components/main-encuesta/main-encuesta.component';
import { TipoEncuestaShellComponent } from './modules/encuesta/components/tipo-encuesta/tipo-encuesta-shell/tipo-encuesta-shell.component';

const routes: Routes = [
  { 
    path: 'encuesta/:idDispositivo/:canalEncuesta', 
    component: MainEncuestaComponent, 
    children: [
        { 
            path: 'preguntas', 
            loadChildren: () => import('./modules/encuesta/components/tipo-encuesta/tipo-encuestas.module').then(m => m.tipoEncuestaModule) 
        }
    ]
}
];


 export const APP_ROUTES = RouterModule.forRoot(routes, { useHash: true, relativeLinkResolution: 'legacy' });
// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }
