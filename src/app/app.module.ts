import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { APP_ROUTES } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './modules/encuesta/common/common.module';
import { tipoEncuestaModule } from './modules/encuesta/components/tipo-encuesta/tipo-encuestas.module';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MainEncuestaComponent } from './modules/encuesta/components/main-encuesta/main-encuesta.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';


@NgModule({
  declarations: [
    AppComponent,
    MainEncuestaComponent,  
  ],
  imports: [
    BrowserModule,
    APP_ROUTES,
    CommonModule,
    //AppRoutingModule,
    CoreModule,
    HttpClientModule,
    tipoEncuestaModule,
    NgSelectModule,
    ReactiveFormsModule,
    FormsModule,
    NgxIntlTelInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
