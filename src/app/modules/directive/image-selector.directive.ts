import { Directive, ElementRef, Input } from '@angular/core';
import { ImageProviderService } from '../encuesta/common/services/image-provider.service';

@Directive({
    selector: '[appImageSelector]'
})
export class ImageSelectorDirective {

    private defaultName: string = '';
    private defaultType: string = '';
    private hasError: boolean = false;
    private name: string = '';
    private type: string = '';


    @Input()
    public set defaultImageKey(defaultImageKey: string) {
        this.defaultName = defaultImageKey;
    }

    @Input()
    public set defaultImageType(defaultImageType: string) {
        this.defaultType = defaultImageType;
    }

    @Input()
    public set imageKey(imageKey: string) {
        this.name = imageKey;
        this.changeImageSource();
    }

    @Input()
    public set imageType(imageType: string) {
        this.type = imageType;
        this.changeImageSource();
    }

    @Input()
    public set error(error: boolean) {
        this.hasError = error;
        this.changeImageSource();
    }




    constructor(
        private element: ElementRef,
        private imageProviderService: ImageProviderService) { }

    private changeImageSource(isDefault?: boolean): void {
        let image = null;
        if (isDefault) {
            image = this.imageProviderService.getImage(this.defaultType, this.defaultName, this.hasError);
        } else {
            image = this.imageProviderService.getImage(this.type, this.name, this.hasError);
        }
        const splitted = image.split('.');
        const extension = splitted[splitted.length - 1];

        if (String(extension).toLowerCase() === 'svg' && this.element.nativeElement.tagName !== 'IMG') {
            const element = this.element.nativeElement as HTMLObjectElement;
            element.data = image;

            // When the image data is loaded, add the svg styles
            element.addEventListener('load', () => {
                const svg = element.getSVGDocument();
                const path = svg.querySelector('path');
                // Get the current svg color0
                const inactiveColor = path.getAttribute('fill');
                const activeColor = '#FF9900';

                // Set the pointer on all image
                path.style.cursor = 'pointer';
                path.parentElement.style.cursor = 'pointer';
                path.parentElement.parentElement.style.cursor = 'pointer';
                path.parentElement.parentElement.parentElement.style.cursor = 'pointer';
                path.parentElement.parentElement.parentElement.parentElement.style.cursor = 'pointer';

                // For div click function
                //svg.onclick = () => element.click();
                path.onclick = () => element.click();
                path.parentElement.onclick = () => element.click();
                path.parentElement.parentElement.onclick = () => element.click();
                path.parentElement.parentElement.parentElement.onclick = () => element.click();

                // Change styles on hover and blur of parent element (div container)
                element.parentElement.onmouseover = () => path.setAttribute('fill', activeColor);
                element.parentElement.onmouseleave = () => path.setAttribute('fill', inactiveColor);

            });
        } else {
            const element = this.element.nativeElement as HTMLImageElement;
            element.src = image;
        }
    }

}
