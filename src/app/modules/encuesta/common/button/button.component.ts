import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() public text: string = '';
  @Input() public disabled: boolean = true;
  @Output() clickHandler: EventEmitter<any>;
  @Input() public step: number = 0;

  constructor() {
    this.clickHandler = new EventEmitter();
   }

  ngOnInit(): void {
  }

  public $clickHandler(event: Event): void {
    if(!this.disabled){
      this.clickHandler.emit(event);
    }
  }

}
