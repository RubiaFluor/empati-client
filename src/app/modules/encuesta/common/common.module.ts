import { CommonModule, NgClass } from "@angular/common";
import { NgModule } from "@angular/core";
import { ButtonComponent } from "./button/button.component";
import { LayoutSideLeftComponent } from './layout-side-left/layout-side-left.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { NgSelectModule } from "@ng-select/ng-select";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { InputControlComponent } from './input-control/input-control.component';
import { NpsControlComponent } from './nps-control/nps-control.component';
import { ImageSelectorDirective } from '../../directive/image-selector.directive';


@NgModule({
    declarations:[
        ButtonComponent,
        LayoutSideLeftComponent,
        DropdownComponent,
        InputControlComponent,
        NpsControlComponent,
        ImageSelectorDirective
    ],
    imports:[
      CommonModule,
      NgSelectModule,
      ReactiveFormsModule,
      FormsModule, 
    ],
    exports: [
    ButtonComponent,
    InputControlComponent,
    DropdownComponent,
    LayoutSideLeftComponent,
    NpsControlComponent
]
})

export class CoreModule {}