import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DropdownComponent),
      multi: true
    }
  ]
})
export class DropdownComponent implements  ControlValueAccessor {
  @Input() public label: string = '';
  @Input() public tooltipDescription = '';
  @Input() public placeholder: string = '';
  @Input() public items: any[] = [];
  @Input() public bindLabel: string = '';
  @Input() public bindValue: string = '';
  @Input() public requiredField: boolean = false;
  @Input() public searchable = false;
  @Input() public clearable = true;
  @Input() public isDisabled: boolean = false;

  public isControlDisabled: boolean = false;
  public value: string = '';

  public _onChange: any;
  public _onTouched: any;
  public _disabled: boolean = false;
  public _value: any;

  constructor() { }


  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this._disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this._value = obj;
  }

}
