import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-input-control',
  templateUrl: './input-control.component.html',
  styleUrls: ['./input-control.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputControlComponent),
      multi: true
    }
  ]
})
export class InputControlComponent implements OnInit , ControlValueAccessor {

  @Input() public type: string = '';
  @Input() public placeholder: string = '';
  @Input() public class: any;
  @Input() public label: string = '';
  @Input() public name: any;
  @Input() public id: any;
  
  value: string = '';
  onChange = (_: any) => { };
  onTouch = () => { };
  isDisabled: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) : void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean) : void {
    this.isDisabled = isDisabled;
  }

  public onInput($event: any) : void {
    this.value = $event.target.value;
    this.onTouch();
    this.onChange(this.value);
  }
}
