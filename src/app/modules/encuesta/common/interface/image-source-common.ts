export interface ImageSourceCommon {
    name: string;
    type: string;
    extension: string;
    key: string;
    project: string;
    startDate?: string | null;
    endDate?: string | null;
}
