import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-nps-control',
  templateUrl: './nps-control.component.html',
  styleUrls: ['./nps-control.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NpsControlComponent),
      multi: true
    }
  ]
})
export class NpsControlComponent implements OnInit, ControlValueAccessor {

  @Input() public style: any;
  @Input() public type: string = '';
  @Input() public label: string = '';
  @Input() public placeholder: string = '';
  @Input() public valor: string = '';
  @Input() public imageKey: any;
  @Input() public imageType: any;
  
  value: any;
  onChange = (_: any) => { };
  onTouch = () => { };

  constructor() { }

  ngOnInit(): void {
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) : void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean) : void {
   // this.isDisabled = isDisabled;
  }

}
