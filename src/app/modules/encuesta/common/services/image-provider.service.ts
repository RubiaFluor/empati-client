import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { IimageSource } from "./../interface/image-source";

@Injectable({
  providedIn: 'root'
})
export class ImageProviderService {

  private readonly IMG_PATH = '../../assets/img';
  private readonly IMAGES : IimageSource[] = [];
  private $context: string = '';
  private get context(): string {
    return this.$context;
  }
  private set context(value: string) {
    this.$context = value;
    // TODO Logica para gatillar cambio de imagen
  }

  constructor() {
   }

  public getImage(imageType: string, imageKey: string, hasError?: boolean): string {
    const CURRENT_PROJECT = 'empati'; // TODO: Obtain the project by service

    // If the request comes by error, returns the default path
    if (hasError) {
      return this.getDefaultImage(imageType, imageKey, CURRENT_PROJECT);
    }

    // const image = this.IMAGES.find((img) =>
    // Number(img.startDate.split('/')[1]) === (moment().month() + 1) && img.key === imageKey);;

    const image = this.IMAGES.find(img => img.name);

    if (image) {
      const imgPath = `${this.IMG_PATH}/${image.type}/${image.project}-${image.key}-${image.name}.${image.extension}`;
      console.log(imgPath)
      return imgPath;
    }

    return this.getDefaultImage(imageType, imageKey, CURRENT_PROJECT);
  }

  public getDefaultImage(imageType: string, imageKey: string, project: string): string {
    // TODO: Change the extension
    return `${this.IMG_PATH}/${imageType}/${project}-${imageKey}-default.png`;
  }

  public setContext(value: string): void {
    if (value && value !== this.context) {
      this.context = value;
    }
  }

}
