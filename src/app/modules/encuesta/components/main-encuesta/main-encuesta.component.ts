import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EncuestaService } from '../../services/encuesta.service';

@Component({
  selector: 'app-main-encuesta',
  templateUrl: './main-encuesta.component.html',
  styleUrls: ['./main-encuesta.component.scss']
})
export class MainEncuestaComponent implements OnInit {

  
  public preguntas: any;
  public idDispositivo: string = '';
  public tipoCanal: string = 'URL'

  constructor(
    public encuestaService: EncuestaService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.paramMap.subscribe((params: any) => {
      if (params.params.idDispositivo) {
        this.idDispositivo = params.params.idDispositivo;
        this.tipoCanal = params.params.canalEncuesta;
      } else {
        console.error("La uri consultada no es válida!");
        // this.cargando = false;
        return;
      }

      encuestaService.getEncuestasByDispositivo(this.idDispositivo)
        .subscribe(resultado => {
          encuestaService.idDispositivo = this.idDispositivo;
          encuestaService.canalEncuesta = this.tipoCanal;

          if (resultado.status.code === 200) { //CLOYOLA
            router.navigate(['/preguntas']);
          }
          else {
            console.error("Encuesta consultada no es válida.");
            this.router.navigate(['/encuesta-no-encontrada']);
          }
        },
          // Error al comunicarse con el servidor
          error => {
            console.error("Error al comunicarse con el servidor");
            this.router.navigate(['/encuesta-no-encontrada']);
            //this.cargando = false;
          }

        )
    })
  }

  ngOnInit(): void {
  }

}
