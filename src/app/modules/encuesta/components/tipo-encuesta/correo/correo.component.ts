import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-correo',
  templateUrl: './correo.component.html',
  styleUrls: ['./correo.component.scss']
})
export class CorreoComponent implements OnInit {

  public formCorreo: FormGroup;

  constructor(
    private fb: FormBuilder
  ) {
    this.formCorreo = this.fb.group({
      correo: ['', Validators.required]
    })
   }

  ngOnInit(): void {
    
  }

}
