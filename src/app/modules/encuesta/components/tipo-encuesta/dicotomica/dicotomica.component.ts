import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-dicotomica',
  templateUrl: './dicotomica.component.html',
  styleUrls: ['./dicotomica.component.scss']
})
export class DicotomicaComponent implements OnInit {

  public formDicotomica: FormGroup;
  constructor(
    private fb: FormBuilder
  ) {
    this.formDicotomica = this.fb.group({
      respuestaDicotomica: ['', Validators.required]
    })
   }

  ngOnInit(): void {
  }

}
