import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-desplegable',
  templateUrl: './lista-desplegable.component.html',
  styleUrls: ['./lista-desplegable.component.scss']
})
export class ListaDesplegableComponent implements OnInit {

  public items: any;
  constructor() { }
  ngOnInit(): void {
    this.items = [
      {
        id:1,
        opcion:"Revistas"
      },
      {
        id:2,
        opcion:"Libros"
      },
      {
        id:3,
        opcion:"Papiros"
      }
    ]
  }

}
