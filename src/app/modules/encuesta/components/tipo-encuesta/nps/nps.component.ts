import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-nps',
  templateUrl: './nps.component.html',
  styleUrls: ['./nps.component.scss']
})
export class NpsComponent implements OnInit {

  public readonly STYLE_NPS = "background-color:#E9211E;margin:10px"
  @Input() pregunta: any;
  public respuesta: any;
  public scoreNPS: any;
  
  constructor() {
    this.scoreNPS = [
      { id:1, valueOne:"1"},
      { id:2, value:"2"},
      { id:3, value:"3"},
      { id:4, value:"4"},
      { id:5, value:"5"}
   ];
  }

  ngOnInit(): void {
  }

 public seleccionarRespuesta(res: any) : void{
    this.respuesta = res;
    let respuesta = {
      idPregunta: this.pregunta.idPregunta,
      idTipoPregunta: this.pregunta.idTipoPregunta,
      respuesta: res
    }
  }

}
