import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-numero',
  templateUrl: './numero.component.html',
  styleUrls: ['./numero.component.scss']
})
export class NumeroComponent implements OnInit {

  public formNumero: FormGroup;
  constructor(
    private fb: FormBuilder
  ) {
    this.formNumero = this.fb.group({
      respuestaNumero: ['', Validators.required]
    })
   }

  ngOnInit(): void {
  }

}
