import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-observacion',
  templateUrl: './observacion.component.html',
  styleUrls: ['./observacion.component.scss']
})
export class ObservacionComponent implements OnInit {

  public formObservacion: FormGroup;

  constructor(
    private fb: FormBuilder
  ) {
    this.formObservacion = this.fb.group({
      reclamo: new FormControl(''),
      sugerencia: new FormControl(''),
      felicitacion: new FormControl(''),
      observacion: new FormControl(''),
      nombre: new FormControl(''),
      correo: new FormControl('')
    })
  }

  ngOnInit(): void {
  }

}
