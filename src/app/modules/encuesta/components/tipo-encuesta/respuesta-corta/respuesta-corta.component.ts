import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-respuesta-corta',
  templateUrl: './respuesta-corta.component.html',
  styleUrls: ['./respuesta-corta.component.scss']
})
export class RespuestaCortaComponent implements OnInit {

  public formRespuestaCorta: FormGroup;
  constructor(
    private fb: FormBuilder
  ) {
    this.formRespuestaCorta = this.fb.group({
      respuestaCorta: ['', Validators.required]
    })
  }

  ngOnInit(): void {
  }

}
