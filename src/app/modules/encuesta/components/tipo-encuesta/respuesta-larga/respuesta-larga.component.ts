import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-respuesta-larga',
  templateUrl: './respuesta-larga.component.html',
  styleUrls: ['./respuesta-larga.component.scss']
})
export class RespuestaLargaComponent implements OnInit {

  public formRespuestaLarga: FormGroup;
  constructor(
    private fb: FormBuilder
  ) {
    this.formRespuestaLarga = this.fb.group({
      respuestaLarga: ['', Validators.required]
    })
   }

  ngOnInit(): void {
  }

}
