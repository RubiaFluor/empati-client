import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-seleccion-multiple',
  templateUrl: './seleccion-multiple.component.html',
  styleUrls: ['./seleccion-multiple.component.scss']
})
export class SeleccionMultipleComponent implements OnInit {

  @Input() public opciones: any;
  @Input() public pregunta: any;

  constructor() { }

  ngOnInit(): void {
  }

}
