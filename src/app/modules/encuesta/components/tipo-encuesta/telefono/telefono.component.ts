import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CountryISO, PhoneNumberFormat, SearchCountryField } from 'ngx-intl-tel-input';

@Component({
  selector: 'app-telefono',
  templateUrl: './telefono.component.html',
  styleUrls: ['./telefono.component.scss']
})
export class TelefonoComponent implements OnInit {

  public phoneForm: FormGroup;

  public separateDialCode: boolean;
  public searchCountryField: any;
  public tooltipLabel: any;
  public countryISO: any;
  public preferredCountries: CountryISO[];
  public phoneNumberFormat: any;
  public customPlaceholder: string;
  public regexTelefono = '\D*([+56]\d [2-9])(\D)(\d{4})(\D)(\d{4})';

  constructor(private fb : FormBuilder) { 
    this.phoneForm = this.fb.group({
      phone: ['']
    })
    
    this.separateDialCode = true;
    this.searchCountryField = SearchCountryField;
    this.countryISO = CountryISO;
    this.preferredCountries = [CountryISO.Chile, CountryISO.Argentina];
    this.phoneNumberFormat = PhoneNumberFormat;
    this.customPlaceholder = "Buscar"

  }

  ngOnInit(): void {
  }

}
