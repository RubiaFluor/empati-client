import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IResponseEncuesta } from '../../../interfaces/encuesta';
import { EncuestaService } from '../../../services/encuesta.service';

@Component({
  selector: 'app-tipo-encuesta-shell',
  templateUrl: './tipo-encuesta-shell.component.html',
  styleUrls: ['./tipo-encuesta-shell.component.scss']
})
export class TipoEncuestaShellComponent implements OnInit {

  public preguntas : any;
  public step: number = 1;

  constructor(
    public encuestaService: EncuestaService
  ) {
  }

  ngOnInit(): void {
    this.getEncuestas();
  }

  public getEncuestas(): void {
    this.preguntas = this.encuestaService.dataEncuesta?.data;
    console.log(this.preguntas)
  }

  public follow() {
    this.step++;
  }

  public back() {
    this.step--;
  }


}
