import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule, Routes } from "@angular/router";
import { CoreModule } from "../../common/common.module";
import { CsatComponent } from "./csat/csat.component";
import { NpsComponent } from "./nps/nps.component";
import { NumeroComponent } from "./numero/numero.component";
import { TelefonoComponent } from "./telefono/telefono.component";
import { TipoEncuestaShellComponent } from "./tipo-encuesta-shell/tipo-encuesta-shell.component";
import { SeleccionMultipleComponent } from './seleccion-multiple/seleccion-multiple.component';
import { ListaDesplegableComponent } from './lista-desplegable/lista-desplegable.component';
import { FechaComponent } from './fecha/fecha.component';
import { CorreoComponent } from './correo/correo.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RespuestaCortaComponent } from './respuesta-corta/respuesta-corta.component';
import { RespuestaLargaComponent } from './respuesta-larga/respuesta-larga.component';
import { DicotomicaComponent } from './dicotomica/dicotomica.component';
import { WebComponent } from './web/web.component';
import { ObservacionComponent } from './observacion/observacion.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const routes: Routes = [
    {
        path: 'preguntas', component: TipoEncuestaShellComponent
    }
]

@NgModule({
    declarations: [
        TipoEncuestaShellComponent,
        CsatComponent,
        NpsComponent,
        NumeroComponent,
        TelefonoComponent,
        SeleccionMultipleComponent,
        ListaDesplegableComponent,
        FechaComponent,
        CorreoComponent,
        RespuestaCortaComponent,
        RespuestaLargaComponent,
        DicotomicaComponent,
        WebComponent,
        ObservacionComponent
    ],
    imports: [
        BrowserModule,
        CoreModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        NgxIntlTelInputModule,
        BrowserAnimationsModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        TipoEncuestaShellComponent
    ]
})

export class tipoEncuestaModule { }