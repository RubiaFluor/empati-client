import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-web',
  templateUrl: './web.component.html',
  styleUrls: ['./web.component.scss']
})
export class WebComponent implements OnInit {

  public formWeb: FormGroup;

  constructor(
    private fb: FormBuilder
  ) { 
    this.formWeb = this.fb.group({
      web: ['', Validators.required]
    })
  }

  ngOnInit(): void {
  }

}
