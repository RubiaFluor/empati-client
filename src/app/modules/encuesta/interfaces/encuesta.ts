export interface IResponseEncuesta {
    idPregunta: string,
    idTipoPregunta: string,
    opciones: [],
    pregunta: string,
    tipoPregunta: string
}