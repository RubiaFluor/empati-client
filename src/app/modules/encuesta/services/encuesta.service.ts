import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EncuestaService {
  private baseUrl: string;

  public dataEncuesta: any;
  public logoCliente: any;
  public idDispositivo: string = '';
  public canalEncuesta: string = 'URL'

  constructor(
    private httpClient: HttpClient,
    private _sanitizer: DomSanitizer) {
    this.baseUrl = environment.encuesta;
  }

  public getEncuestasByDispositivo(idDispositivo: string): Observable<any> {
    const url = `${this.baseUrl}muestra/encuesta/${idDispositivo}`;
    return this.httpClient.get(url)
      .pipe(
        map((result: any) => {
          let observation = {
            idPregunta: "obs",
            tipoPregunta: "OBS",
            pregunta: 'Escribe una observación (opcional)'
          }
          result.data.push(observation);
          let dataFinal = Object.assign(result.data, { 'idDispositivo': idDispositivo });
          this.dataEncuesta = result;
          this.logoCliente = result.data.logoClient ? this._sanitizer.bypassSecurityTrustResourceUrl(result.data.logoClient) : './assets/img/logo-empati-dark.png';
          this.dataEncuesta.data = dataFinal;
          return this.dataEncuesta;
        }, () => {
          console.error();
        }
        ),
        catchError(error => throwError(error))
      )
  }
}
